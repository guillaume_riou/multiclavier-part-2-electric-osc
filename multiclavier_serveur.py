import sys

print("""Usage : python multiclavier_client.py [OPTION]...
    --server-port PORT  the port of the osc server (1234 if unspecified)
                        this is the port the clients will have to use when connecting
    --client-port PORT  the port of the osc client (4321 if unspecified)
                        this is the port on which to broadcast answers back to clients
    --mouse BACKEND     <pyinput|xdotool> (pyinput if unspecified)
                        pyinput is better, but xdotool works in openmw (and lego island 2 and probably other games :P )
    --turn DURATION     turn duration (in seconds, defaults to 0.5)
    --decay TURNS       how many turns of idle are tolerated before clients are evicted (defaults to 3)

    Dependencies:
        pygame, pythonosc, pynput (or xdotool in the PATH), keyboard

        On linux, the user must be in the input group.
        You can try `sudo dump-keys.sh` if there are key mapping issues.
""")

server_port = 1234
client_port = 4321
mouse_backend_name = "pyinput"
control_swap_interval = 0.5
decay = 3

i = 1
while i < len(sys.argv):
    arg = sys.argv[i]
    if arg == "--server-port":
        i += 1
        server_port = int(sys.argv[i])
    elif arg == "--client-port":
        i += 1
        client_port = int(sys.argv[i])
    elif arg == "--mouse":
        i += 1
        mouse_backend_name = sys.argv[i]
    elif arg == "--turn":
        i += 1
        control_swap_interval = float(sys.argv[i])
    elif arg == "--decay":
        i += 1
        decay = int(sys.argv[i])
    elif arg == "--help" or arg == "-h":
        sys.exit()
    else:
        print(f"Unrecognized arg \"{sys.argv[i]}\"")
    i += 1

from threading import Thread, Lock
from time import sleep
import pygame
import pygame.freetype
import keyboard
from copy import deepcopy
from pythonosc.dispatcher import Dispatcher
from pythonosc import osc_server, udp_client
import os
from common import InputStates, colors

pygame.init()

if mouse_backend_name == "xdotool":
    print("Using xdotool for mouse backend")
    from subprocess import Popen
    class XDOTOOLMouse():
        def press(self, btn):
            Popen(["xdotool", "mousedown", str(btn)])
        def release(self, btn):
            Popen(["xdotool", "mouseup", str(btn)])
        def move(self, x, y):
            Popen(["xdotool", "mousemove_relative", "--", str(x), str(y)])
        def scroll(self, x, y):
            # Horizontal scrolling is unsupported
            y = int(y * 2)
            if y < 0:
                Popen(["xdotool", "click", "--repeat", str(-y), "5"])
            elif y > 0:
                Popen(["xdotool", "click", "--repeat", str(y), "4"])
    mouse = XDOTOOLMouse()
else:
    print("Using pyinput for mouse backend")
    from pynput.mouse import Button, Controller
    conversion_pynput_mouse = {1:Button.left, 2:Button.middle, 3:Button.right}
    class PynputMouse():
        def __init__(self):
            self.mouse = Controller()
        def press(self, btn):
            self.mouse.press(conversion_pynput_mouse[btn])
        def release(self, btn):
            self.mouse.release(conversion_pynput_mouse[btn])
        def move(self, x, y):
            self.mouse.move(x, y)
        def scroll(self, x, y):
            self.mouse.scroll(x, y)
    mouse = PynputMouse()

if os != "nt":
    import linux_duck

pygame.font.init()
font = pygame.freetype.Font("Montserrat-Medium.ttf", 16)

free_ids = list(range(10))
clients = {}
clients_lock = Lock()
class Client():
    def __init__(self, client_name):
        self.input_states = InputStates()
        self.last_input = 0
        self.active = False
        self.id = free_ids.pop()
        (self.text_surface, self.text_rect) = font.render(
            client_name,
            fgcolor=(255, 255, 255))
        (self.text_surface_selected, _) = font.render(
            client_name,
            fgcolor=colors[self.id])

server_input_states = InputStates()
def switch_to_input_states(input_states):
    global server_input_states

    old_keys = set(server_input_states.keys.keys())
    new_keys = set(input_states.keys.keys())
    for key in old_keys - new_keys:
        send_key_event(key, False)
    for key in new_keys - old_keys:
        send_key_event(key, True)

    for i in range(1, 4):
        old_btn = server_input_states.buttons & 1 << i
        new_btn = input_states.buttons & 1 << i
        if old_btn != new_btn:
            send_mouse_event(i, new_btn != 0)

    # Mouse and wheel are relative and consumed immediately, so switching doesn't care about them.
    # deepcopy is probably overkill but whatever
    server_input_states = deepcopy(input_states)

def send_key_event(key_entry, key_state):
    try:
        key = key_entry[0] if os.name == "nt" or len(key_entry) < 2 else key_entry[1]
        keyboard.send(key, key_state, not key_state)
    # sometimes we receive something strange but we don't want the server to choke on it.
    except:
        pass

def send_mouse_event(button_number, button_state):
    if button_state:
        mouse.press(button_number)
    else:
        mouse.release(button_number)

def handle_osc(address, *args):
    address_splitted = [x for x in address.split("/") if x ]
    client_name = address_splitted[0]
    if client_name not in clients:
        with clients_lock:
            clients[client_name] = Client(client_name)
    client = clients[client_name]

    msg_type = address_splitted[1]
    if msg_type == "input":
        buttons, motion_x, motion_y, wheel_x, wheel_y, active_keys = args
        keys = { tuple(key): True for key in active_keys }
        client.input_states = InputStates(buttons, (motion_x, motion_y), (wheel_x, wheel_y), keys)
        client.last_input = 0
        # Wheel and motion are relative, and should be consumed immediately.
        # When switching between clients, they are meaningless.
        if client.active:
            if client.input_states.wheel != (0, 0):
                mouse.scroll(*client.input_states.wheel)
            if client.input_states.motion != (0, 0):
                mouse.move(*client.input_states.motion)
            switch_to_input_states(client.input_states)
    # Virtual keyboards don't support handling press/down, so they can only send keys to input immediately
    elif msg_type == "key":
        if client.active:
            keyboard.press_and_release(args[0])
    elif msg_type == "write":
        if client.active:
            for car in args[0]:
                if car == " ":
                    car = "space"
                if (car.isalnum() and car.islower()) or car == "space":
                    keyboard.press_and_release(car)
                else:
                    keyboard.write(args[0], exact=True)
                    break

print("server listening on", server_port)
dispatcher = Dispatcher()
dispatcher.map("*", handle_osc)
server = osc_server.ThreadingOSCUDPServer(
    ("0.0.0.0", server_port), dispatcher)
osc_server_thread = Thread(target=lambda:server.serve_forever())
osc_server_thread.start()

print("broadcasting back to clients on", client_port)
osc_client = udp_client.SimpleUDPClient("255.255.255.255", client_port, allow_broadcast=True)

screen = pygame.display.set_mode((160, 160))

current_control_id = 0
polling_interval = 1/60
control_swap_frame = 0

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            server.server_close()
            pygame.quit()
            sys.exit()

    if control_swap_frame <= 0:
        control_swap_frame += control_swap_interval / polling_interval
        screen.fill((0, 0, 0))

        if len(clients) > 0:
            current_control_id = (current_control_id+1) % len(clients)

            # Render client names
            to_remove = []
            with clients_lock:
                for i, name in enumerate(clients):
                    client = clients[name]
                    if current_control_id == i:
                        screen.blit(client.text_surface_selected, (16, 16 * (i+1)))
                        client.last_input = client.last_input + 1
                        osc_client.send_message(f"/active_client", (name, client.id))
                        client.active = True
                        if client.last_input > decay:
                            to_remove.append(name)
                        else:
                            switch_to_input_states(client.input_states)
                    else:
                        screen.blit(client.text_surface, (16, 16 * (i+1)))
                        client.active = False
            # Remove stale clients
            with clients_lock:
                for name in to_remove:
                    free_ids.append(clients[name].id)
                    del clients[name]
        else:
            switch_to_input_states(InputStates())

        pygame.display.flip()
    control_swap_frame -= 1

    sleep(polling_interval)
