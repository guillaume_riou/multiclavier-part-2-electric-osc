import pygame
import colorsys

class InputStates():
    def __init__(self, buttons = 0, motion = (0, 0), wheel = (0, 0), keys = {}):
        self.buttons = buttons
        self.motion = motion
        self.wheel = wheel
        self.keys = keys

norm_colors = [colorsys.hsv_to_rgb(i * 0.3, 1, 1) for i in range(10)]
colors = [(r * 255, g * 255, b * 255) for (r, g, b) in norm_colors]
