import re
import keyboard
from keyboard._nixkeyboard import cleanup_key, register_key, to_name, from_name, keypad_scan_codes

# Ya daonkaey
keyboard._nixkeyboard.ensure_root = lambda: None
keyboard._nixcommon.ensure_root = lambda: None

# Shamelessly copypastaed
def build_tables():
    if to_name and from_name: return

    modifiers_bits = {
        'shift': 1,
        'alt gr': 2,
        'ctrl': 4,
        'alt': 8,
    }
    keycode_template = r'^keycode\s+(\d+)\s+=(.*?)$'
    dump = open("dump-keys-only.txt").read()
    for str_scan_code, str_names in re.findall(keycode_template, dump, re.MULTILINE):
        scan_code = int(str_scan_code)
        for i, str_name in enumerate(str_names.strip().split()):
            modifiers = tuple(sorted(modifier for modifier, bit in modifiers_bits.items() if i & bit))
            name, is_keypad = cleanup_key(str_name)
            register_key((scan_code, modifiers), name)
            if is_keypad:
                keypad_scan_codes.add(scan_code)
                register_key((scan_code, modifiers), 'keypad ' + name)

    # dumpkeys consistently misreports the Windows key, sometimes
    # skipping it completely or reporting as 'alt. 125 = left win,
    # 126 = right win.
    if (125, ()) not in to_name or to_name[(125, ())] == 'alt':
        register_key((125, ()), 'windows')
    if (126, ()) not in to_name or to_name[(126, ())] == 'alt':
        register_key((126, ()), 'windows')

    # The menu key is usually skipped altogether, so we also add it manually.
    if (127, ()) not in to_name:
        register_key((127, ()), 'menu')

    synonyms_template = r'^(\S+)\s+for (.+)$'
    dump = open("dump-long-info.txt").read()
    for synonym_str, original_str in re.findall(synonyms_template, dump, re.MULTILINE):
        synonym, _ = cleanup_key(synonym_str)
        original, _ = cleanup_key(original_str)
        if synonym != original:
            from_name[original].extend(from_name[synonym])
            from_name[synonym].extend(from_name[original])

keyboard._nixkeyboard.build_tables = build_tables
