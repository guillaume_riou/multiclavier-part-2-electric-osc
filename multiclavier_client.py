import sys

print("""Usage : python multiclavier_client.py [DISPLAY NAME] [SERVER IP] [OPTION]...
    --client-port PORT  the port of the osc client (1234 if unspecified)
                        this is the port on which this client communicates with the server
    --server-port PORT  the port of the osc server (4321 if unspecified)
                        this server is used when receiving messages back from the server

    Dependencies:
        pygame, pythonosc

        On linux, the user must be in the input group.
        You can try `sudo dump-keys.sh` if there are key mapping issues.
""")

if len(sys.argv) < 3:
    sys.exit()

debug = False
this_client_name = sys.argv[1]
server_ip = sys.argv[2]
client_port = 1234
server_port = 4321

i = 3
while i < len(sys.argv):
    arg = sys.argv[i]
    if arg == "--server-port":
        i += 1
        server_port = int(sys.argv[i])
    elif arg == "--client-port":
        i += 1
        client_port = int(sys.argv[i])
    elif arg == "--debug":
        i += 1
        debug = True
    else:
        print(f"Unrecognized arg \"{sys.argv[i]}\"")
    i += 1

from threading import Thread
from time import sleep
import pygame
from pythonosc.dispatcher import Dispatcher
from pythonosc import udp_client, osc_server
from common import InputStates, colors
import os
if os != "nt":
    import linux_duck

osc_client = udp_client.SimpleUDPClient(server_ip, client_port)
client_is_active = False
active_client_id = None

pygame.init()

# receives information from server about current activity state
def handle_osc(address, *args):
    global client_is_active, active_client_id
    client_is_active = args[0] == this_client_name
    active_client_id = args[1]


dispatcher = Dispatcher()
dispatcher.map("*", handle_osc)
server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", server_port), dispatcher)
osc_server_thread = Thread(target=lambda:server.serve_forever())
osc_server_thread.start()

infoObject = pygame.display.Info()
surface = pygame.display.set_mode((160, 160))

class MouseStates():
    def __init__(self):
        self.button = 0
        self.motion = (0, 0)
        self.wheel = (0, 0)

input_states = InputStates()

import keyboard

def send():
    # Clean keys in case of ghostly apparitions. This breaks certain keys, but fuck this.
    new_keys = {}
    for key in input_states.keys:
        try:
            if keyboard.is_pressed(key):
                new_keys[key] = True
        except:
            pass
    input_states.keys = new_keys
    #input_states.keys = { key: True for key in input_states.keys if keyboard.is_pressed(key) }
    if debug and any(input_states.keys):
        print(", ".join([name + ": " + str(scan_code) for name, scan_code in input_states.keys]), flush=True)
    osc_client.send_message(f"/{this_client_name}/input", (
        input_states.buttons, *input_states.motion, *input_states.wheel,
        [list(key) for key in input_states.keys]))

    # Motion and wheel are relative, and thus "consumed" when sent
    input_states.motion = (0, 0)
    input_states.wheel = (0, 0)

def print_pressed_keys(e):
    key = (e.name, e.scan_code)
    if e.event_type == "down":
        input_states.keys[key] = True
    elif key in input_states.keys:
        del input_states.keys[key]
    send()

keyboard.hook(print_pressed_keys)

pygame.event.set_grab(True)
pygame.mouse.set_visible(False)
while True:
    dirty = False
    for event in pygame.event.get():
        if event.type in [pygame.MOUSEBUTTONDOWN, pygame.MOUSEBUTTONUP] and event.button in [4,5]:
            continue
        elif event.type == pygame.MOUSEBUTTONDOWN:
            input_states.buttons |= 1 << (event.button)
            dirty = True
        elif event.type == pygame.MOUSEMOTION:
            input_states.motion = (event.rel[0] + input_states.motion[0], event.rel[1] + input_states.motion[1])
            dirty = True
        elif event.type == pygame.MOUSEBUTTONUP:
            input_states.buttons &= ~(1 << event.button)
            dirty = True
        elif event.type == pygame.MOUSEWHEEL:
            input_states.wheel = (event.x + input_states.wheel[0], event.y + input_states.wheel[1])
            dirty = True
        elif event.type == pygame.QUIT:
            server.server_close()
            pygame.quit()
            sys.exit()
    if dirty:
        send()

    if client_is_active:
        pygame.draw.rect(surface, colors[active_client_id], pygame.Rect(0, 0, 160, 160))
    else:
        pygame.draw.rect(surface, (0,0,0), pygame.Rect(0, 0, 160, 160))
    pygame.display.flip()

    sleep(1 / 120)
